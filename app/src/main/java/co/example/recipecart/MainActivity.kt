package co.example.recipecart

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.example.recipecart.activity.CartActivity
import co.example.recipecart.adapter.RecipeListAdapter
import co.example.recipecart.models.Recipe
import co.example.recipecart.viewmodels.RecipeListViewModel


class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    lateinit var progressBar: ProgressBar
    lateinit var retryButton: Button
    lateinit var recipeListAdapter: RecipeListAdapter

    private lateinit var recipeListViewModel: RecipeListViewModel

    companion object {
        var recipes: ArrayList<Recipe> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recipeListViewModel = ViewModelProviders.of(this).get(RecipeListViewModel::class.java)

        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressBar)
        retryButton = findViewById(R.id.errorButton)

        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recipeListAdapter =
            RecipeListAdapter(this, recipes)
        recyclerView.adapter = recipeListAdapter

        setObservers()
        recipeListViewModel.getRecipes()
    }

    private fun setObservers() {
        recipeListViewModel.recipes.observe(this, Observer {
            it.let {
                recipes.addAll(it)
                recipeListAdapter.notifyDataSetChanged()
            }
        })

        recipeListViewModel.error.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        recipeListViewModel.loading.observe(this, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.GONE
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.cart) {
            startActivity(Intent(this, CartActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}