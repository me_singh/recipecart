package co.example.recipecart.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.example.recipecart.AppHelper
import co.example.recipecart.R
import co.example.recipecart.models.CartItem
import co.example.recipecart.models.Recipe
import com.bumptech.glide.Glide


class RecipeListAdapter(val context: Context, val recipeList: List<Recipe>) : RecyclerView.Adapter<RecipeListAdapter.RecipeListViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeListViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.single_recipe_layout, parent, false)
        return RecipeListViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: RecipeListViewHolder, position: Int) {
        val recipe = recipeList.get(holder.adapterPosition)

        holder.name.text = recipe.name
        holder.price.text = recipe.price

        if (recipe.image != null) {
            Glide.with(context)
                .load(recipe.image)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_foreground)
                .into(holder.image)
        }

        holder.addToCart.setOnClickListener {
            AppHelper.addToCart(context, CartItem(recipe, 1))
        }
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }

    class RecipeListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val addToCart: ImageButton = itemView.findViewById(R.id.addToCart)
        val image: ImageView = itemView.findViewById(R.id.image)
        val price: TextView = itemView.findViewById(R.id.price)
    }
}