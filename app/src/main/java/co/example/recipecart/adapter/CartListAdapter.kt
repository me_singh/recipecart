package co.example.recipecart.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.example.recipecart.AppHelper
import co.example.recipecart.R
import co.example.recipecart.interfaces.OnCartListChange
import co.example.recipecart.models.CartItem


class CartListAdapter(val context: Context) : RecyclerView.Adapter<CartListAdapter.CartListAdapterViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartListAdapterViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.single_cart_item, parent, false)
        return CartListAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartListAdapterViewHolder, position: Int) {
        val cartItem = AppHelper.cartItems.get(holder.adapterPosition)

        holder.name.text = cartItem.recipe?.name
        holder.price.text = cartItem.recipe?.price
        holder.count.text = cartItem.count.toString()

        holder.increaseCnt.setOnClickListener {
            AppHelper.increaseRecipeCount(context, cartItem)
            notifyItemChanged(holder.adapterPosition)
            (context as OnCartListChange).updateCartAmount()
        }
        holder.decreaseCnt.setOnClickListener {
            AppHelper.decreaseRecipeCount(context, cartItem)
            notifyItemChanged(holder.adapterPosition)
            (context as OnCartListChange).updateCartAmount()
        }
    }

    override fun getItemCount(): Int {
        return AppHelper.cartItems.size
    }

    class CartListAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val price: TextView = itemView.findViewById(R.id.price)
        val count: TextView = itemView.findViewById(R.id.count)
        val increaseCnt: ImageButton = itemView.findViewById(R.id.increaseCnt)
        val decreaseCnt: ImageButton = itemView.findViewById(R.id.decreaseCnt)
    }
}