package co.example.recipecart.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.example.recipecart.repository.RecipeListRepository
import co.example.recipecart.models.Recipe
import com.samples.flickrsample.utils.ResultResponse
import kotlinx.coroutines.launch
import java.lang.Error
import java.lang.Exception

class RecipeListViewModel(app: Application) : AndroidViewModel(app) {

    private val recipeRepository =
        RecipeListRepository()

    private var _recipes = MutableLiveData<List<Recipe>>()
    val recipes: LiveData<List<Recipe>>
        get() = _recipes

    private var _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    private var _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading


    fun getRecipes() {
        try {
            viewModelScope.launch {
                _loading.postValue(true)
                val res = recipeRepository.getRecipes()
                if (res is ResultResponse.Success) {
                    _recipes.postValue(res.data)
                    _loading.postValue(false)
                } else {
                    _error.postValue((res as Error).message)
                    _loading.postValue(false)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}