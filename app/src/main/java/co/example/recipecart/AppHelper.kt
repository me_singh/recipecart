package co.example.recipecart

import android.content.Context
import android.widget.Toast
import co.example.recipecart.models.CartItem
import co.example.recipecart.models.Recipe
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppHelper {

    companion object {

        val BASE_URL = "https://s3-ap-southeast-1.amazonaws.com"
        const val JSON_FILE_NAME = "reciped9d7b8c.json"

        val cartItems = ArrayList<CartItem>()

        fun getRetrofitInstance() : Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun addToCart(context: Context, cartItem: CartItem): Boolean {
            if (cartItems.contains(cartItem)) {
                Toast.makeText(context, "Already Added", Toast.LENGTH_SHORT).show()
                return false
            }
            cartItems.add(cartItem)
            Toast.makeText(context, "Recipe Added", Toast.LENGTH_SHORT).show()
            return true
        }

        fun removeFromCart(context: Context, cartItem: CartItem): Boolean {
            if (cartItems.contains(cartItem)) {
                cartItems.remove(cartItem)
            }
            Toast.makeText(context, "Recipe Removed", Toast.LENGTH_SHORT).show()
            return true
        }

        fun increaseRecipeCount(context: Context, cartItem: CartItem) {
            if (cartItems.contains(cartItem)) {

                val index = cartItems.indexOf(cartItem)
                if (index != -1) {
                    var cartItem = cartItems.get(index)
                    cartItems.remove(cartItem)
                    cartItem.count++
                    cartItems.add(cartItem)
                }
                Toast.makeText(context, "Recipe Increased", Toast.LENGTH_SHORT).show()
            }
        }

        fun decreaseRecipeCount(context: Context, cartItem: CartItem) {
            if (cartItems.contains(cartItem)) {

                val index = cartItems.indexOf(cartItem)
                if (index != -1) {
                    var cartItem = cartItems.get(index)
                    cartItems.remove(cartItem)
                    if (cartItem.count > 1) {
                        cartItem.count++
                        cartItems.add(cartItem)
                    }
                }
                Toast.makeText(context, "Recipe Decreased", Toast.LENGTH_SHORT).show()
            }
        }
    }
}