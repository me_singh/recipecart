package co.example.recipecart.apiservices

import co.example.recipecart.AppHelper
import co.example.recipecart.models.Recipe
import retrofit2.Response
import retrofit2.http.GET

interface RecipeApiService {

    @GET("he-public-data/" + AppHelper.JSON_FILE_NAME)
    suspend fun getRecipes() : Response<List<Recipe>>

}