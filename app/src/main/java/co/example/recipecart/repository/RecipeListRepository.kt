package co.example.recipecart.repository

import co.example.recipecart.AppHelper
import co.example.recipecart.apiservices.RecipeApiService
import com.samples.flickrsample.utils.ResultResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import co.example.recipecart.models.Recipe

class RecipeListRepository {

    private val recipeApiService = AppHelper.getRetrofitInstance()
        .create(RecipeApiService::class.java)

    suspend fun getRecipes() : ResultResponse<List<Recipe>> {

        try {
            return withContext(Dispatchers.IO) {
                val result = recipeApiService.getRecipes()
                if (result.isSuccessful) {
                    if (result.body() != null) {
                        return@withContext ResultResponse.Success(result.body()!!)
                    } else {
                        return@withContext ResultResponse.Error(Error("No Data From Server!"))
                    }
                } else {
                    return@withContext ResultResponse.Error(Error("Server Error!"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ResultResponse.Error(Error("Server Error!"))

    }
}