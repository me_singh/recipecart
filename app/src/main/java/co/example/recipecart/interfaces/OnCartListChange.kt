package co.example.recipecart.interfaces

interface OnCartListChange {
    fun updateCartAmount()
}