package co.example.recipecart.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class CartItem(val recipe: Recipe? = null, var count: Int = 0) : Parcelable {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CartItem

        if (recipe != other.recipe) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recipe?.hashCode() ?: 0
        result *= 31
        return result
    }


}