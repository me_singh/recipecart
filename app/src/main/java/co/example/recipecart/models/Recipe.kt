package co.example.recipecart.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Recipe(val id: Int? = null, val name: String? = null, val image: String? = null, val category: String? = null,
             val label: String? = null, val price: String? = null, val description: String? = null) : Parcelable