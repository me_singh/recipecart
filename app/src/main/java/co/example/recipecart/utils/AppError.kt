package com.samples.flickrsample.utils

import java.lang.Error

class AppError(): Error() {

    var msg: String? = null
    var code: String? = null

    constructor(msg: String, code: String) : this() {
        this.msg = msg
        this.code = code
    }

}