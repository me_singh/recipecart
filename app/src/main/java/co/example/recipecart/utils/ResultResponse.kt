package com.samples.flickrsample.utils

sealed class ResultResponse<out T : Any> {
    data class Success<out T : Any>(val data: T) : ResultResponse<T>()
    data class Error(val error: java.lang.Error) : ResultResponse<Nothing>()
}