package co.example.recipecart.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.example.recipecart.AppHelper
import co.example.recipecart.adapter.CartListAdapter
import co.example.recipecart.R
import co.example.recipecart.interfaces.OnCartListChange
import java.lang.NumberFormatException

class CartActivity : AppCompatActivity(), OnCartListChange {

    lateinit var recyclerView: RecyclerView
    lateinit var cartListAdapter: CartListAdapter
    lateinit var payAmount: Button
    lateinit var totalAmount: TextView
    lateinit var amountBanner: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        recyclerView = findViewById(R.id.recyclerView)
        totalAmount = findViewById(R.id.totalAmount)
        payAmount = findViewById(R.id.payAmount)
        amountBanner = findViewById(R.id.amountBanner)
        recyclerView.layoutManager = LinearLayoutManager(this)
        cartListAdapter = CartListAdapter(this)
        recyclerView.adapter = cartListAdapter

        updateCartAmount()
    }

    override fun updateCartAmount() {
        var amountPayable = 0f
        try {
            for(item in AppHelper.cartItems) {
                amountPayable += (item.recipe?.price?.toFloat()!! * item.count)
            }
            amountBanner.visibility = View.VISIBLE
            totalAmount.text = "TOTAL AMOUNT PAYABLE: Rs $amountPayable"

            payAmount.setOnClickListener {  }

        } catch (e: NumberFormatException) {
            amountBanner.visibility = View.GONE
            e.printStackTrace()
        }
    }

}